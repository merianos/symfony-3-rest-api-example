<?php

namespace spec\AppBundle\Handler;

use AppBundle\Entity\User;
use AppBundle\Handler\HandlerInterface;
use AppBundle\Handler\UserHandler;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserHandlerSpec extends ObjectBehavior {
	private $repository;

	function let( UserRepositoryInterface $repository ) {
		$this->repository = $repository;

		$this->beConstructedWith($repository);
	}

	function it_is_initializable() {
		$this->shouldHaveType( UserHandler::class );
		$this->shouldImplement( HandlerInterface::class );
	}

	function it_can_GET() {
		$id = 191;
		$this->get($id);
		$this->repository->find($id)->shouldHaveBeenCalled();
	}

	function it_cannot_get_ALL() {
		$this->shouldThrow( '\DomainException' )->during( 'all' );
	}

	function it_cannot_POST() {

	}

	function it_cannot_PUT() {

	}

	function it_should_allow_PATCH(User $user) {

	}

	function it_should_throw_if_PATCH_not_given_valid_user() {

	}

	function it_cannot_DELETE() {

	}
}
