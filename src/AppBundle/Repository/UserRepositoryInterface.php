<?php
/**
 * Created by PhpStorm.
 * User: merianos
 * Date: 6/3/2018
 * Time: 10:33 πμ
 */

namespace AppBundle\Repository;

/**
 * Interface UserRepositoryInterface
 * @package AppBundle\Repository
 */
interface UserRepositoryInterface {

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function find($id);
}
