<?php

namespace AppBundle\Controller;

use AppBundle\Handler\UserHandler;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserController
 *
 * @package AppBundle\Controller
 * @Rest\RouteResource("users")
 */
class UserController extends FOSRestController implements ClassResourceInterface {
	/**
	 * Get a single User
	 *
	 * @ApiDoc(
	 *     output="AppBundle\Entity\User",
	 *     statusCodes={
	 *          200 = "Returned when successfull",
	 *          400 = "Returned when not found"
	 *     }
	 * )
	 *
	 * @param  int $userId The user id.
	 *
	 * @throws NotFoundHttpException when does not exists
	 *
	 * @return View
	 */
	public function getAction( $userId ) {
		$user = $this->getUserHandler()->get($userId);
		//$user = $this->getDoctrine()->getRepository('AppBundle:User')->find($userId);
		$view = $this->view($user);

		return $view;
	}

	/**
	 * Gets a collection of Users
	 *
	 * @ApiDoc(
	 *     output="AppBundle\Entity\User",
	 *     statusCodes={ 405 = "Method not allowed" }
	 * )
	 *
	 * @throws MethodNotAllowedHttpException
	 *
	 * @return void
	 */
	public function cgetAction() {
		throw new MethodNotAllowedHttpException( [], "Method not allowed" );
	}

	/**
	 * @return \AppBundle\Handler\UserHandler|object
	 */
	private function getUserHandler() {
		return $this->container->get('crv.handler.user_handler');
	}
}
