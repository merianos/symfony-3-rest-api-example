<?php
/**
 * Created by PhpStorm.
 * User: merianos
 * Date: 6/3/2018
 * Time: 10:07 πμ
 */

namespace AppBundle\Handler;


use AppBundle\Repository\UserRepositoryInterface;

class UserHandler implements HandlerInterface {

	private $repository;

	public function __construct( UserRepositoryInterface $repository ) {
		$this->repository = $repository;
	}

	/**
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function get( $id ) {
		return $this->repository->find( $id );
	}

	/**
	 * @param int $limit
	 * @param int $offset
	 *
	 * @return mixed
	 */
	public function all( $limit, $offset ) {
		throw new \DomainException( 'This method is not implemented' );
	}

	/**
	 * @param array $parameters
	 * @param array $options
	 *
	 * @return mixed
	 */
	public function post( array $parameters, array $options ) {
		// TODO: Implement post() method.
	}

	/**
	 * @param mixed $resource
	 * @param array $parameters
	 * @param array $options
	 *
	 * @return mixed
	 */
	public function put( $resource, array $parameters, array $options ) {
		// TODO: Implement put() method.
	}

	/**
	 * @param mixed $resource
	 * @param array $parameters
	 * @param array $options
	 *
	 * @return mixed
	 */
	public function patch( $resource, array $parameters, array $options ) {
		// TODO: Implement patch() method.
	}

	/**
	 * @param mixed $resource
	 *
	 * @return mixed
	 */
	public function delete( $resource ) {
		// TODO: Implement delete() method.
	}
}
