<?php

namespace spec\AppBundle\Repository\Doctrine;

use AppBundle\Repository\Doctrine\UserRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserRepositorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UserRepository::class);
    }
}
