<?php
/**
 * Created by PhpStorm.
 * User: merianos
 * Date: 6/3/2018
 * Time: 10:55 πμ
 */

namespace AppBundle\Security\Authorization\Voter;


use AppBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class UserVoter {
	const VIEW = 'view';

	/**
	 * @param $attribute
	 *
	 * @return bool
	 */
	public function supportsAttribute( $attribute ) {
		return in_array(
			$attribute,
			array(
				self::VIEW,
			)
		);
	}

	/**
	 * @param $class
	 *
	 * @return bool
	 */
	public function supportsClass( $class ) {
		$supportedClass = UserInterface::class;

		return $supportedClass === $class || is_subclass_of( $class, $supportedClass );
	}

	/**
	 * @param TokenInterface $token
	 * @param                $requestedUser
	 * @param array          $attributes
	 *
	 * @return int
	 */
	public function vote( TokenInterface $token, $requestedUser, array $attributes ) {
		if ( ! $this->supportsClass( get_class( $requestedUser ) ) ) {
			return VoterInterface::ACCESS_ABSTAIN;
		}

		$attribute = $attributes[0];

		if ( ! $this->supportsAttribute( $attribute ) ) {
			return VoterInterface::ACCESS_ABSTAIN;
		}

		$loggedInUser = $token->getUser();

		if ( $loggedInUser === $requestedUser ) {
			return VoterInterface::ACCESS_GRANTED;
		}

		return VoterInterface::ACCESS_DENIED;
	}
}
