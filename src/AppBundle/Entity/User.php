<?php

namespace AppBundle\Entity;

use AppBundle\Model\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class User
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserEntityRepository")
 * @ORM\Table(name="user")
 * @Serializer\ExclusionPolicy("all")
 */
class User extends BaseUser implements UserInterface {

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="UUID")
	 * @ORM\Column(type="guid")
	 * @Serializer\Expose()
	 * @Serializer\Type("string")
	 */
	protected $id;

	/**
	 * @Serializer\Expose()
	 * @Serializer\Type("string")
	 */
	protected $email;

	/**
	 * @Serializer\Expose()
	 * @Serializer\Type("string")
	 */
	protected $username;
}
