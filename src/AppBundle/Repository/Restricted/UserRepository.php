<?php

namespace AppBundle\Repository\Restricted;

use AppBundle\Repository\Doctrine\UserRepository as DoctrineUserRepository;
use AppBundle\Repository\UserRepositoryInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserRepository implements UserRepositoryInterface {

	/**
	 * @var DoctrineUserRepository
	 */
	private $user_repository;
	/**
	 * @var AuthorizationCheckerInterface
	 */
	private $authorization_checker;

	public function __construct(
		DoctrineUserRepository $user_repository,
		AuthorizationCheckerInterface $authorization_checker
	) {
		$this->user_repository = $user_repository;
		$this->authorization_checker = $authorization_checker;
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function find( $id ) {
		$user = $this->user_repository->find( $id );

		$this->denyAccessUnlessGranted( 'view', $user );

		return $user;
	}

	protected function denyAccessUnlessGranted(
		$attribute,
		$object = null,
		$message = 'Access Denied'
	) {
		if ( ! $this->authorization_checker->isGranted( $attribute, $object ) ) {
			throw new AccessDeniedException( $message );
		}
	}
}
