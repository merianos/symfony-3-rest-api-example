<?php

namespace spec\AppBundle\Repository\Restricted;

use AppBundle\Repository\Restricted\UserRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserRepositorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UserRepository::class);
    }
}
